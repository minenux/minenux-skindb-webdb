<?php
/**!
 * @package   minenux-skindb-webdb
 * @filename  lists.php controller
 * @route     >api>v1>lists
 * @version   1.0
 */
class notFoundApi_model extends model {

    public function show($errormessage = 'function and/or argument not found or parameters are incorrect') {
    
        $variables = array('message'=>$errormessage,'httpcode'=>'404');
        $this->renderOutput($variables,'json');
    }

}

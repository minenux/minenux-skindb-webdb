<?php
/**!
 * @package   minenux-skindb-webdb
 * @filename  api/api.php controller
 * @route     >api
 * @version   1.0
 */
class skins_controller extends controller {

  public function execute() {
        $this->model->show();
  }
}

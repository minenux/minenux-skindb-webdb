<?php
/**!
 * @package   minenux-skindb-webdb
 * @filename  index.php controller
 * @route     >index
 * @version   1.0
 */
class index_controller extends controller {

  public function execute() {
        $this->model->show();
  }
}

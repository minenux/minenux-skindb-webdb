<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>SkinsDB</title>
        <link rel="icon" href="icon.png" type="image/x-icon">
        <link rel="stylesheet" href="../assets/css/bulma.min.css">
    </head>
    <body>
        <nav class="navbar" role="navigation" aria-label="main navigation">
          <div class="navbar-brand">
            <a class="navbar-item">
            </a>
            <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
            </a>
          </div>
          <div id="navbarBasicExample" class="navbar-menu">
            <div class="navbar-start">
              <div class="navbar-item has-dropdown is-hoverable">
                <a href="../lists"  class="navbar-link">
                  Start
                </a>
                <div class="navbar-dropdown">
                  <a href="../about"  class="navbar-item">
                    About
                  </a>
                  <a href="#" class="navbar-item is-selected">
                    API
                  </a>
                  <hr class="navbar-divider">
                  <a href="https://codeberg.org/minenux/minenux-skindb-webdb/issues"  class="navbar-item">
                    Report an issue
                  </a>
                </div>
              </div>
            </div>
            <div class="navbar-end">
              <div class="navbar-item">
                  <a class="field is-grouped">
                    <p class="control is-expanded">
                      <input class="input" type="text" placeholder="type here to Search skin">
                    </p>
                    <p class="control">
                      <button class="button is-info is-selected">
                        Search
                      </button>
                    </p>
                  </a>
                  <a href="../about" class="button is-primary">
                    About
                  </a>
                </div>
              </div>
            </div>
          </div>
        </nav>
        <br>
            <main class="contain">
            <h1 class="title">API documentation</h1>
            <hr>
<div class="content">
<!-------------------------------------------------------------------------------------------->
<p dir="auto">The API os skindb is based on two artifacts:</p>
<ul dir="auto">
<li>the "data api" defines the communication format for the data</li>
<li>and "skin format" that defines the nature of data files to use</li>
</ul>
<h2 class="title is-2" id="user-content-player-skin-data-api" dir="auto"><a class="anchor" href="#player-skin-data-api"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" class="svg octicon-link" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a>player skin data api</h2>
<p dir="auto">For api, skin is a couple of two parts, one is the png skin texture file and
the other is the metadata information file.</p>
<p dir="auto">The api will show the information of each skin in json format, each line of the
metadata information file and a last line with base64 skin dump.</p>
<h4 class="title is-4" id="user-content-api-request-format" dir="auto"><a class="anchor" href="#api-request-format"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" class="svg octicon-link" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a>api request format</h4>
<ul dir="auto">
<li>Old way, php site <code>/api/v2/get.json.php?getlist&amp;page=%i&amp;outformat=base64&amp;per_page=%p</code> with <code>%i</code> as number page</li>
<li>New way, astro site <code>/api/v1/content?client=mod&amp;page=%i</code> with <code>%i</code> as number page, only base64 output</li>
</ul>
<p dir="auto">File public assets</p>
<ul dir="auto">
<li>preview asset <code>/skins/%i/%r.png</code>  with <code>%i</code> as id skin and <code>%r</code> as ramdom generation number</li>
</ul>
<h4 class="title is-4" id="user-content-api-message-format" dir="auto"><a class="anchor" href="#api-message-format"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" class="svg octicon-link" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a>api message format</h4>
<pre class="code-block"><code class="chroma language-text display">    {
        "sucess": true,                 // boolean : true | false
        "message": "server message",    // string(32) : a message from the server
        "page": 1,                      // integer : curent page of result, 0 on errors
        "pages": 18,                    // integer : max number of pages
        "per_page": 10,                 // integer : how many results are per page
        "skins": [                      // array : data of the results, skin items on normal operation
            {
                "id": Int,              // integer : same id as original api, new ones has new format
                "name": "String",       // string(16) : alpha numeric only
                "author": "String",     // string(16) : alpha numeric only
                "license": "String",    // string(16) : SPDX licence identifier as https://spdx.org/licenses/
                "type": "String",       // format of the data, if skin, image/png, either just binary
                "img": "String"         // base64 : encoded image content string of skin player, png file
            }
        ]
    }
</code><button class="code-copy ui button" data-clipboard-text="    {
        &quot;sucess&quot;: true,                 // boolean : true | false
        &quot;message&quot;: &quot;server message&quot;,    // string(32) : a message from the server
        &quot;page&quot;: 1,                      // integer : curent page of result, 0 on errors
        &quot;pages&quot;: 18,                    // integer : max number of pages
        &quot;per_page&quot;: 10,                 // integer : how many results are per page
        &quot;skins&quot;: [                      // array : data of the results, skin items on normal operation
            {
                &quot;id&quot;: Int,              // integer : same id as original api, new ones has new format
                &quot;name&quot;: &quot;String&quot;,       // string(16) : alpha numeric only
                &quot;author&quot;: &quot;String&quot;,     // string(16) : alpha numeric only
                &quot;license&quot;: &quot;String&quot;,    // string(16) : SPDX licence identifier as https://spdx.org/licenses/
                &quot;type&quot;: &quot;String&quot;,       // format of the data, if skin, image/png, either just binary
                &quot;img&quot;: &quot;String&quot;         // base64 : encoded image content string of skin player, png file
            }
        ]
    }">
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" class="svg octicon-copy" width="16" height="16" aria-hidden="true"><path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path></svg></button></pre>
<h2 class="title is-2"  id="user-content-player-skin-format" dir="auto"><a class="anchor" href="#player-skin-format"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" class="svg octicon-link" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a>player skin format</h2>
<p dir="auto">The two files of skin are one png and one txt, the txt format depends on api site,
the png is describes in next section.</p>
<ul dir="auto">
<li>The metadata file correspond of the <code>name</code>, <code>author</code>, <code>license</code> fields of json data</li>
<li>The skin image file correspond of the <code>img</code> field of json data</li>
</ul>
<h4 class="title is-4" id="user-content-filename-meta-txt" dir="auto"><a class="anchor" href="#filename-meta-txt"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" class="svg octicon-link" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a>filename meta txt</h4>
<p dir="auto">The defunct skin database (<a href="http://minetest.fensta.bplaced.net" rel="nofollow">http://minetest.fensta.bplaced.net</a>), uses the
a unique format, each line is as of <code>key = "string",</code> with a comma at the end.
if no value is given will be <code>unknow+&lt;id&gt;</code> and for license will be <code>CC-BY-NC-3.0</code></p>
<ul dir="auto">
<li>filename must match the file skin png file
<ul dir="auto">
<li>filename must start with "character" string</li>
<li>filename format must be <code>character_&lt;id&gt;.txt</code></li>
<li>filename skin must be <code>../textures/character_&lt;id&gt;.png</code></li>
</ul>
</li>
<li>file contents must be as:
<ul dir="auto">
<li>name : must be alphanumeric</li>
<li>author : must be alphanumeric</li>
<li>description : must be alphanumeric and maximun of 46 chars</li>
<li>comment : must be alphanumeric and maximun of 46 chars</li>
<li>license : must be alphanumeric and format must be as <a href="https://spdx.org/licenses/" rel="nofollow">https://spdx.org/licenses/</a></li>
</ul>
</li>
</ul>
<pre class="code-block"><code class="chroma language-text display">name = "&lt;name&gt;",
author = "&lt;author&gt;",
description = "&lt;description&gt;",
comment = "&lt;comment&gt;",
license = "&lt;SPDX licence identifier&gt;",
</code><button class="code-copy ui button" data-clipboard-text="name = &quot;<name>&quot;,
author = &quot;<author>&quot;,
description = &quot;<description>&quot;,
comment = &quot;<comment>&quot;,
license = &quot;<SPDX licence identifier>&quot;,"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" class="svg octicon-copy" width="16" height="16" aria-hidden="true"><path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path></svg></button></pre><h4 id="user-content-filename-meta-new" dir="auto"><a class="anchor" href="#filename-meta-new"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" class="svg octicon-link" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a>filename meta new</h4>
<p dir="auto">New sites simplifies the meta file with only lines to parse, this
format manage each line is as of <code>key</code> directly without a comma at the end</p>
<ul dir="auto">
<li>filename must match the file skin png file
<ul dir="auto">
<li>filename must start with "character" string</li>
<li>filename format must be <code>character_&lt;id&gt;.txt</code></li>
<li><strong>BUT filename skin is <code>character.&lt;id&gt;.png</code></strong></li>
</ul>
</li>
<li>file contents must be as:
<ul dir="auto">
<li>line 1 : must be alphanumeric ans is assumed as the skin name only</li>
<li>line 2 : must be alphanumeric and is assumed as the skin author only</li>
<li>line 3 : must be alphanumeric and format must be as <a href="https://spdx.org/licenses/" rel="nofollow">https://spdx.org/licenses/</a></li>
</ul>
</li>
</ul>
<pre class="code-block"><code class="chroma language-text display">&lt;name&gt;
&lt;author&gt;
&lt;SPDX licence identifier&gt;,
</code><button class="code-copy ui button" data-clipboard-text="<name>
<author>
<SPDX licence identifier>,"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" class="svg octicon-copy" width="16" height="16" aria-hidden="true"><path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path></svg></button></pre><h4 id="user-content-png-texture-character" dir="auto"><a class="anchor" href="#png-texture-character"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" class="svg octicon-link" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a>png texture character</h4>
<p dir="auto">The file follow the format of the template description provided by
AntumDeluge at the <a href="https://opengameart.org/content/minetest-character-template" rel="nofollow">https://opengameart.org/content/minetest-character-template</a></p>

<!-------------------------------------------------------------------------------------------->
</div>
            </main>
        <br>
        <footer class="footer">
            <div class="content has-text-centered">
                <p><strong>SkinsDdatabase</strong> by MinenuX</p>
            </div>
        </footer>
    </body>
</html>

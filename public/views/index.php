<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>SkinsDB</title>
        <link rel="icon" href="icon.png" type="image/x-icon">
        <link rel="stylesheet" href="assets/css/bulma.min.css">
    </head>
    <body>
        <nav class="navbar" role="navigation" aria-label="main navigation">
          <div class="navbar-brand">
            <a class="navbar-item">
            </a>
            <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
            </a>
          </div>
          <div id="navbarBasicExample" class="navbar-menu">
            <div class="navbar-start">
              <div class="navbar-item has-dropdown is-hoverable">
                <a href="lists"  class="navbar-link">
                  Start
                </a>
                <div class="navbar-dropdown">
                  <a href="about"  class="navbar-item">
                    About
                  </a>
                  <a href="api/api" class="navbar-item is-selected">
                    API
                  </a>
                  <hr class="navbar-divider">
                  <a href="https://codeberg.org/minenux/minenux-skindb-webdb/issues"  class="navbar-item">
                    Report an issue
                  </a>
                </div>
              </div>
            </div>
            <div class="navbar-end">
              <div class="navbar-item">
                  <a class="field is-grouped">
                    <p class="control is-expanded">
                      <input class="input" type="text" placeholder="type here to Search skin">
                    </p>
                    <p class="control">
                      <button class="button is-info is-selected">
                        Search
                      </button>
                    </p>
                  </a>
                  <a href="about" class="button is-primary">
                    About
                  </a>
                </div>
              </div>
            </div>
          </div>
        </nav>
        <br>
            <main class="contain">
            <h1 class="title">Skins</h1>
            <hr>
            Wellcome!
            </main>
        <br>
        <footer class="footer">
            <div class="content has-text-centered">
                <p><strong>SkinsDdatabase</strong> by MinenuX</p>
            </div>
        </footer>
    </body>
</html>

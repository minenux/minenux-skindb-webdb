/**!
 * @package   ReceiptAPI
 * @filename  00-auth.sql
 * @version   1.0
 * @autor     Díaz Urbaneja Víctor Eduardo Diex <diazvictor@tutamail.com>
 * @date      21.11.2023 17:05:17 -04
 */

drop table if exists users_profiles;
drop table if exists permissions;
drop table if exists profiles;
create table profiles (
	id_profile 	int not null auto_increment primary	key,
	profile 	varchar(32) not null,
	reserved	varchar(255) null
);

insert into profiles
	(profile, beging, ending)
values
	('admin', '00:00:00', 'powered'),
	('minetest', '00:00:00', 'uploader');

drop table if exists modules;
create table modules (
	id_module	int not null auto_increment primary	key,
	module 		varchar(32) not null,
	description	varchar(32) not null,
	unique 		(module)
);

create table permissions (
	id_permission	int not null auto_increment primary key,
	id_profile		int not null,
	id_module		int not null,
	`read`			boolean not null default true,
	`write`			boolean not null default true,
	`update`		boolean not null default true,
	foreign key (id_profile) references profiles(id_profile),
	foreign key (id_module) references modules(id_module)
);

drop table if exists users;
create table users (
    id_user 	int not null auto_increment primary	key,
    username	varchar(16) not null,
    password	char(128) not null,
    type_user	enum('admin', 'minetest'),
    status		boolean not null default true
);

insert into users
	(username, password, type_user)
values
	('jhondoe', '$2y$10$JgaTqfeMUhkii.Sj9NBc8e0NjLeMwmLKAgqGwhiUiw1nK/e7E6VdC', 2),

create table users_profiles (
	id_user		int not null,
	id_profile	int not null,
	foreign key (id_user) references users(id_user),
	foreign key (id_profile) references profiles(id_profile)
);


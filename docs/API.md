# skindb-webdb API reference

Is based on now defunct skin database (http://minetest.fensta.bplaced.net), 
that will be now at https://skindb.sourceforge.io/ but modernized!

The API os skindb is based on two artifacts:
* the "data api" defines the communication format for the data
* and "skin format" that defines the nature of data files to use

## player skin data api

For api, skin is a couple of two parts, one is the png skin texture file and 
the other is the metadata information file.

The api will show the information of each skin in json format, each line of the 
metadata information file and a last line with base64 skin dump.

#### api request format

* Old way, php site `/api/v2/get.json.php?getlist&page=%i&outformat=base64&per_page=%p` with `%i` as number page
* New way, astro site `/api/v1/content?client=mod&page=%i` with `%i` as number page, only base64 output

File public assets 

* preview asset `/skins/%i/%r.png`  with `%i` as id skin and `%r` as ramdom generation number

#### api message format

```
    {
        "sucess": true,                 // boolean : true | false
        "message": "server message",    // string(32) : a message from the server
        "page": 1,                      // integer : curent page of result, 0 on errors
        "pages": 18,                    // integer : max number of pages
        "per_page": 10,                 // integer : how many results are per page
        "skins": [                      // array : data of the results, skin items on normal operation
            {
                "id": Int,              // integer : same id as original api, new ones has new format
                "name": "String",       // string(16) : alpha numeric only
                "author": "String",     // string(16) : alpha numeric only
                "license": "String",    // string(16) : SPDX licence identifier as https://spdx.org/licenses/
                "type": "String",       // format of the data, if skin, image/png, either just binary
                "img": "String"         // base64 : encoded image content string of skin player, png file
            }
        ]
    }
```

## player skin format

The two files of skin are one png and one txt, the txt format depends on api site, 
the png is describes in next section.

* The metadata file correspond of the `name`, `author`, `license` fields of json data
* The skin image file correspond of the `img` field of json data

#### filename meta txt

The defunct skin database (http://minetest.fensta.bplaced.net), uses the 
a unique format, each line is as of `key = "string",` with a comma at the end.
if no value is given will be `unknow+<id>` and for license will be `CC-BY-NC-3.0`

* filename must match the file skin png file
    * filename must start with "character" string
    * filename format must be `character_<id>.txt`
    * filename skin must be `../textures/character_<id>.png`
* file contents must be as:
    * name : must be alphanumeric
    * author : must be alphanumeric
    * description : must be alphanumeric and maximun of 46 chars
    * comment : must be alphanumeric and maximun of 46 chars
    * license : must be alphanumeric and format must be as https://spdx.org/licenses/

```
name = "<name>",
author = "<author>",
description = "<description>",
comment = "<comment>",
license = "<SPDX licence identifier>",
```

#### filename meta new

New sites simplifies the meta file with only lines to parse, this 
format manage each line is as of `key` directly without a comma at the end

* filename must match the file skin png file
    * filename must start with "character" string
    * filename format must be `character_<id>.txt`
    * **BUT filename skin is `character.<id>.png`**
* file contents must be as:
    * line 1 : must be alphanumeric ans is assumed as the skin name only
    * line 2 : must be alphanumeric and is assumed as the skin author only
    * line 3 : must be alphanumeric and format must be as https://spdx.org/licenses/

```
<name>
<author>
<SPDX licence identifier>,
```

#### png texture character

The file follow the format of the template description provided by 
AntumDeluge at the https://opengameart.org/content/minetest-character-template

![template_colormap.png](template_colormap.png)

![template.png](template.png)

In future api can implement a way to check this file!

## API use cases

| workf | issue | use case api refers | ANON USER | AUTH USER  |
| ----- | ----- | ------------------- | --------- | ---------- |
|  1.   |  #5   |  list   skin        |    x      |     x      |
|  2.   |  #4   |   login             |    x      |            |
|  3.   |  #8   |  load   skin        |    x      |     x      |
|  4.   |  #6   |  mark   skin        |           |     x      |
|  5.   |  #7   |  delete skin        |           |     x      |

### api key

The api can receive a key used to retrieve much more data, if there is no apikey 
then only 10 items will be offered by the api, so means that parameters 
to have specific amount of items of skins will be ignored if no api key present.

### list skin

List all skins or filtered skins, this is the default operation and the normal usage for non api key users.

CAll `/api/v1/list.php?getlist&outformat=base64&page=1`

##### Pre condiciones

- anonimous user
- apikey present as optional
- OPTIONAL #4  [login](#login)
    - only one ip can be at the same time per session per user, to avoid conflicts on git filesystem

1. if no filters are show, all the skins will be returned
2. if error happened, the result will be show in message key
3. if apikey is given, user can retrieve all skins at the same time
4. if apikey is missing only 10 skins per page are can retrieve at the same time

- parameters GET or POST
    - `per_page` : OPTIONAL : integer - amount of skins/data items returned in json data/skin
    - `page` : OPTIONAL : integer - the current page of the items to retrieve
    - `apikey` : OPTIONAL : string - alphanumeric 64 chars key for api

##### workflow

* 1 get optional POST parameters
    - `name` : OPTIONAL : String : skin name as filter, of the skin player, 12 chars maximun
    - `author` : OPIONAL : String : skin author as filter, is `username` like, 12 chars maximun
    - `description` : OPTIONAL : String : long details of the skin made, 64 chars maximun
    - `comment` : OPTIONAL : String : short details of the skin made, 32 chars maximun
    - `license` : OPTIONAL : String : SPDX licence identifier as https://spdx.org/licenses/
    - `skin` : OPTIONAL : String : name of file as filter
    - `apikey` : OPTIONAL : String
* 2 check and validations, then
    - search the metadata file as [player skin format](#player-skin-format), use the cache in sqlite database

##### good workflow

* 3 response
    - mensaje
         - if no api key, message will be "skins found, only 10 results per page, no api key present"
         - you must count al the json id found and report as numbered results found

```
    {
        "sucess": true,
        "message": "skins found",
        "page": 1,
        "pages": 2,
        "per_page": 10,
        "skins": [
            {
                "id": Int,
                "name": "String",
                "author": "String",
                "license": "String",
                "type": "String",
                "img": "String",
                "comment": "String"
            }
            {
                "id": Int,
                "name": "String",
                "author": "String",
                "license": "String",
                "type": "String",
                "img": "String",
                "comment": "String"
            }
        ]
    }
```


##### failed workflow

* 3 failed response
    - mensaje json skind data
         - count al the json id found and report as numbered results found
         - miessage will show any error or no skins found

```
    {
        "sucess": false,
        "message": "no skins found",
        "page": 1,
        "pages": 0,
        "per_page": 10,
        "skins": []
    }
```


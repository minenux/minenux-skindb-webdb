# skindb-webdb

Huge Minetest/Multicraft skin database interface!

## Introduction

A huge Minetest/Multicraft skin database interface that makes it possible to search for the skin of your choice. 

This is the web front of huge skin database files at https://codeberg.org/minetest-stuffs/minetest-skindb-skindata
or the prefered site of https://git.minetest.io/minetest-stuffs/minetest-skindb-skindata repository files!

You can change those settings and files on your needs, the default API example 
only handles a tree case, and retrieve the tree by nodes, there is no authentication, 
but for auth support please read [DEVEL.md](DEVEL.md) document file.

## How to use this repo

``` bash
rm -rf $HOME/Devel/minenux-skindb-webdb && mkdir $HOME/Devel

git clone --recursive https://codeberg.org/minenux/minenux-skindb-webdb $HOME/Devel/minenux-skindb-webdb

cd $HOME/Devel/minenux-skindb-webdb &&  git submodule foreach git checkout develop && git submodule foreach git pull

./server-sample
```

## How to start a module

``` bash
cd $HOME/Devel/minenux-skindb-webdb

./new_module public newlinktoshow
```

* `controllers/newlinktoshow.php`
* `models/newlinktoshow.php`
* `http://localhost:<port>/newlinktoshow`

## How to Deploy and develop

Start geany an browse the `Devel/minenux-skindb-webdb` directory , look 
for `skindb-webdb` proyect file, load into Geany!

Then read the [DEVEL.md](DEVEL.md) for some specific details.

#### Requisitos

* Linux:
  * Debian 7+ Alpine 3.12+
  * git 2.0+
  * php 5+ 7+ o 8+
* database
  * sqlite3 / perconadb 5.7+

## LICENSE

The Guachi Framework is open-source software under the MIT License, this downstream part is a reduced version for!
Este minicore conteine partes del framework Banshee bajo la misma licencia.

* (c) 2023 Dias Victor @diazvictor

El proyecto minenux-skindb-webdb es open source y free software bajo la licencia **CC-BY-SA-NC** Compartir igual sin derecho comercial a menos que se pida permiso esten de acuerdo ambas partes, y con atribuciones de credito.

* (c) 2023 PICCORO Lenz McKAY <mckaygerhard>
* (c) 2023 Dias Victor @diazvictor


# skindb-webdb API reference

Is based on now defunct skin database (http://minetest.fensta.bplaced.net), 
that will be now at https://skindb.sourceforge.io/

This documents the proposals, may change or not to be made

### login

This case is still in proposal, cos we are thinking to use git only

#### pre conditions

* 1 send POST parameters
    - `username` : REQUIRED : github/codeberg/gitlab it depends
    - `userpass` : REQUIRED : contraseña entre 6 y 20 caracteres alfanumericos
    - `userkey` : REQUIRED : palabra de 60 caracteres alfanumerica

##### workflow

* 2 check DB api parameters, the database is sqlite

##### good workflow

* 3 response sucess
    - mensaje json user data
         - id : internal id of the user
         - name : human name if any
         - username : must match the POST username
         - userkey : must match the POST userkey, this is the api key
         - type : 0 or 1, 1 is admin user
         - apikey : same as userkey

```
    {
        "sucess": true,
        "message": "auth success"
        "data": [
            {
                "id": Int,
                "name": "String",
                "username": "String",
                "userkey": "String",
                "type": Int,
                "apikey": "String"
            }
        ]
    }
```

* NOTE only one ip can be at the same time per session per user, to avoid conflicts in git filesystem

##### failed workflow

* 2 response fails
    - mensaje json de datos incorrectos session invalida

```
    {
        "sucess": false,
        "message": "auth fail"
        "data": [
            {
                ".. invalid api key"
            }
        ]
    }
```

### load skin

This case is still in proposal, cos we are thinking to use git only

##### Pre conditions

- #4  [login](#login)
- only one ip can be at the same time per session per user, to avoid conflicts on git filesystem

* 1 send POST parameters with data
    - `name` : REQUIRED : String : skin name, of the skin player, 12 chars maximun
    - `author` : REQUIRED : String : auto filled with `username` session, 12 chars maximun
    - `description` : OPTIONAL : String : long details of the skin made, 64 chars maximun
    - `comment` : OPTIONAL : String : short details of the skin made, 32 chars maximun
    - `license` : REQUEIRED : String : SPDX licence identifier as https://spdx.org/licenses/
    - `skin` : REQUIRED : Base64 : png file
    - `apikey` : REQUIRED : string - alphanumeric 64 chars key for api
    - `username` : REQUIRED : user session if login

##### workflow

* 2 check and validations, then
    - create the metadata file as [player skin format](#player-skin-format)
    - decode base64 png file and dump into temp filesystem

##### good workflow

* 3 create id skin and store into git
    - create new id format YYYYMMDDHHmmss
    - save the metadata file into filesystem git submodule as in [data files](#data-files)
    - save the png dump file into filesystem git submodule as in [data files](#data-files)
    - save the metadata into sqlite database for cache search as in [data files](#data-files)
    - schedule commit to the git filesystem
* 4 Response

```
    {
        "sucess": true,
        "message": "skin stored",
        "page": 1,
        "pages": 1,
        "per_page": 10,
        "skins": [
            {
                "id": Int,
                "name": "String",
                "author": "String",
                "license": "String",
                "type": "String",
                "img": "String",
                "comment": "String"
            }
        ]
    }
```


##### failed workflow

* 3 failed response

```
    {
        "sucess": false,
        "message": "skin error data",
        "page": 1,
        "pages": 1,
        "per_page": 10,
        "skins": [
            {
                "id": Int,
                "name": "String",
                "author": "String",
                "license": "String",
                "type": "String",
                "img": "String",
                "comment": "String"
            }
        ]
    }
```



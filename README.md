# Skindb-webdb

Huge Minetest/Multicraft skin database interface!

## Introduction

A huge Minetest/Multicraft skin database interface that makes it possible to search for the skin of your choice. 
this repository handle the sourcecode of the skin database web interface at https://skindb.sourceforge.io/

This is the web front of huge skin database files at https://codeberg.org/minetest-stuffs/minetest-skindb-skindata
or the prefered site of https://git.minetest.io/minetest-stuffs/minetest-skindb-skindata repository files!

## HOW TO

This uses the GUACHI mini api framework, please refers to [docs/README.md](docs/README.md)

## LICENSE

The Guachi Framework is open-source software under the MIT License, this downstream part is a reduced version for!
Este minicore conteine partes del framework Banshee bajo la misma licencia.

* (c) 2023 Dias Victor @diazvictor

This proyect is CC-BY-SA-NC 

* (c) 2023 PICCORO Lenz McKAY <mckaygerhard>
* (c) 2023 Dias Victor @diazvictor
* (c) 2023 Tyron Lucero

